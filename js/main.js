

$(document).ready(function () {


	// $(".icon-bar").click(function () {
	// 	$("header#header").toggleClass("active-menu");
	// });
	$(".toggle-search").click(function () {
		$(".wrap-search-header").toggleClass("active");
	});
	$(".title-search-category").click(function () {
		$(".wrap-list-category-search").toggleClass('active');
		$(".wrap-list-category-search").css('transition', 'all .4s');
	});
	$(document).mouseup(function (e) {
		var container = $(".select-category");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.wrap-list-category-search').removeClass('active');
		}
	});

	$(".more-product").click(function () {
		$(".wrap-info-product").removeClass("box-min-height");
		$(this).css('display','none');
	});

	function setWidthText() {
		let showChar = 350;
		var ellipsestext = "...";
		var data = ['box-content-ykien p'];
		data.forEach(function (value) {

			$('.' + value).each(function () {
				var content = $(this).html();
				if (content.length > showChar) {
					var c = content.substr(0, showChar);
					var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp';
					$(this).html(html);
				}

			});
		});
	}
	setWidthText();

	var count = 1;
	$("#minus").click(function () {
		if (count > 1) {
			count--;
			$('#number-product').val(count);
			if($("#minus").hasClass('remove-minus')){
				$('#minus').removeClass('remove-minus')
			}
		} else {
			$('#minus').addClass('remove-minus')
		}
	});
	$("#plus").click(function () {
			count++;
			$('#number-product').val(count);
			$('#minus').removeClass('remove-minus')
	});



	// menu 

	$(".wrap-icon-toggle").click(function () {
		$(this).toggleClass('active');
		$('body').toggleClass('overlay');
		$('nav#nav').toggleClass('active');
		$('header#header').toggleClass('active-menu');
		$('main#main').toggleClass('active-menu');
		$('footer#footer').toggleClass('active-menu');
	});
	$(document).mouseup(function (e) {
		var container = $("nav#nav, header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$('.wrap-icon-toggle').removeClass('active');
				$('body').removeClass('overlay');
				$('nav#nav').removeClass('active');
				$('header#header').removeClass('active-menu');
				$('main#main').removeClass('active-menu');
				$('footer#footer').removeClass('active-menu');
		}
	});
	function resize(){
		var win = $(window).width();
		if(win < 991){
			$('.title-category').click(function(){
				$(this).toggleClass('active');
				$('.wrap-list-category').slideToggle();
			});
			$('.icon-arrow-category').click(function(){
				$(this).siblings('.menu-child').slideToggle();
			})
		}

		if(win < 1025){
			$('.icon-arrow-menu').click(function(){
				$(this).toggleClass('active');
				$(this).siblings('.menu-child').slideToggle();
			})
		}
	}
	resize();
	$(window).on('resize', function(){
        resize();
    });

});
$('#wrap-video, #slide-banner-main-index').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		},
	}
});
$('#slide-blog-more').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	margin: 25,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		},
		390: {
			items: 2,
			margin: 15,
		},
		768: {
			items: 3,
		},
	}
});
$('#slide-image-item').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	margin: 25,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 2,
			margin:15
		},
		411: {
			items: 2,
			margin:15
		},
		768: {
			items: 3,
		},
		991: {
			items: 4,
		},
	}
});
$('#slide-image-product').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	margin: 15,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 2,
		},
		411: {
			items: 3,
		},
		768: {
			items: 2,
		},
		991: {
			items: 3,
		},
	}
});


// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
	} else {
		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
	};
});

$('#why-choose').waypoint(function () {
	$(".counter").each(function () {
		var $this = $(this),
			countTo = $this.attr("data-countto");
		countDuration = parseInt($this.attr("data-duration"));
		$({ counter: $this.text() }).animate(
			{
				counter: countTo
			},
			{
				duration: countDuration,
				easing: "linear",
				step: function () {
					$this.text(Math.floor(this.counter));
				},
				complete: function () {
					$this.text(this.counter);
				}
			}
		);
	});
}, {
	offset: '100%'
});

// scroll tabs 

$(document).ready(function () {
	$("#myScrollspy a").on('click', function (event) {
		if (this.hash !== "") {
			event.preventDefault();

			var hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function () {

				window.location.hash = hash;
			});
		}
	});
});
